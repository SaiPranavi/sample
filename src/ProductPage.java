import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class ProductPage {
    String name,review,quantity;
    WebDriver driver;
    public ProductPage(WebDriver driver){
        this.driver=driver;
    }

    public void viewSpecifications(){
        driver.findElement(By.xpath("//a[@href='#tab-specification']")).click();
    }
    public void clickReviewTab(){
        driver.findElement(By.xpath("//a[@href='#tab-review']")).click();

    }
    public void enterName(String name){
        WebElement enter_name = driver.findElement(By.cssSelector("input#input-name.form-control"));
        enter_name.sendKeys(name);
    }
    public void writeReview(String review){
        WebElement enter_review =driver.findElement(By.cssSelector("textarea#input-review.form-control"));
        enter_review.sendKeys(review);
    }
    public void selectRating(String rating){
        driver.findElement(By.xpath(".//input[@value='"+rating+"']")).click();
    }
    public void clickContinueButton(){
        driver.findElement(By.cssSelector("button#button-review.btn.btn-primary")).click();
    }
    public void addCurrentItemToWishList(){
        driver.findElement(By.cssSelector(".btn-group .btn.btn-default")).click();

    }
    public void addCurrentItemToCompare(){
        driver.findElement(By.cssSelector("i.fa.fa-exchange")).click();
    }
    public void selectQuantity(String quantity){
        WebElement enter_quantity = driver.findElement(By.cssSelector("input#input-quantity.form-control"));
        enter_quantity.clear();
        enter_quantity.sendKeys(quantity);
    }
    public void clickOnTweet(){
        driver.findElement(By.cssSelector("a.addthis_button_tweet.at300b")).click();

    }
    public void selectShare(){
        driver.findElement(By.cssSelector("a.atc_s.addthis_button_compact")).click();
    }
    public void addToCart(){
        driver.findElement(By.cssSelector("button#button-cart.btn.btn-primary.btn-lg.btn-block")).click();
    }
}
