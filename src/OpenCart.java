import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Set;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
public class OpenCart {
    @Test
    public void testPage() throws Exception {
        WebDriver driver;
        String item, name, review, quantity, website_url, currency, rating;
        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String ConfigPath = rootPath + "cart.txt";
        Properties properties = new Properties();
        properties.load(new FileInputStream(ConfigPath));
        currency = properties.getProperty("currency");
        item = properties.getProperty("item");
        name = properties.getProperty("name");
        review = properties.getProperty("review");
        quantity = properties.getProperty("quantity");
        rating = properties.getProperty("rating");
        website_url = properties.getProperty("website_url");
        driver = new FirefoxDriver();
        driver.get(website_url);
        Assert.assertEquals(website_url, driver.getCurrentUrl());
        HomePage homePage = new HomePage(driver);
        Assert.assertEquals("Your Store", driver.getTitle());
        homePage.checkCurrency(currency);
        homePage.enterItemIntoSearchBar(item);
        homePage.clickOnSearch();
        SearchPage searchPage = new SearchPage(driver);
        searchPage.selectReqItem();
        ProductPage productPage = new ProductPage(driver);
        Assert.assertEquals(item, driver.getTitle());
        productPage.viewSpecifications();
        productPage.clickReviewTab();
        productPage.enterName(name);
        productPage.writeReview(review);
        productPage.selectRating(rating);
        productPage.clickContinueButton();
        productPage.addCurrentItemToWishList();
        productPage.addCurrentItemToCompare();
        productPage.selectQuantity(quantity);
        productPage.addToCart();
        productPage.clickOnTweet();
        Set<String> allHandles = driver.getWindowHandles();
        String currentWindowHandle = allHandles.iterator().next();
        allHandles.remove(currentWindowHandle);
        String lastHandle = allHandles.iterator().next();
        WebDriverWait wait = new WebDriverWait(driver.switchTo().window(lastHandle), 10);
        wait.until(ExpectedConditions.titleIs("Share a link on Twitter"));
        Assert.assertEquals("Share a link on Twitter",driver.switchTo().window(lastHandle).getTitle());
    }
}
