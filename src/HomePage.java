import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class HomePage {
    String item,name,review,quantity;
    WebDriver driver;
    public HomePage(WebDriver driver){
        this.driver=driver;
    }

    public void checkCurrency(String currency){
        driver.findElement(By.cssSelector(".btn.btn-link.dropdown-toggle")).click();
        if(currency.equals("Euro")) {
            driver.findElement(By.xpath(".//button[@name=\"EUR\"]")).click();
        }
        else if(currency.equals("Pound Sterling")) {
            driver.findElement(By.xpath("button[@name=\"GBP\"]")).click();
        }
        else {
            driver.findElement(By.xpath(".//button[@name=\"USD\"]")).click();
        }
    }

    public String enterItemIntoSearchBar(String item){
        WebElement search = driver.findElement(By.cssSelector("input.form-control.input-lg"));
        search.sendKeys(item);
        return item;
    }
    public void clickOnSearch(){
        driver.findElement(By.cssSelector("i.fa.fa-search")).click();

    }
    public void myAccount(){

    }
}
